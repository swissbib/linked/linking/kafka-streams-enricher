/*
 * streams application to link data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.Klaxon
import java.io.File

class IdentifierExtractionFailed(message: String) : Exception(message)

class NoProviderFoundException(message: String) : Exception(message)

fun loadProviders(): Providers {
    val file = File("/configs/providers.json")
    return if (file.isFile)
        Klaxon().parse<Providers>(file)!!
    else
        Klaxon().parse<Providers>(ClassLoader.getSystemResourceAsStream("providers.json")!!)!!
}
