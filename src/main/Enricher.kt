/*
 * kafka-streams-enricher: Enriches swissbib data resources with external data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import org.apache.logging.log4j.Logger
import java.util.*
import kotlin.collections.HashMap

class Enricher(private val mergeConfiguration: MergeConfiguration, properties: Properties, private val log: Logger) {


    private val providers = loadProviders()
    private val jsonParser = JsonParser(log)


    private val viafIndex = ElasticIndex(properties.getProperty("elastic.search.viaf"), properties, log)
    private val gndIndex = ElasticIndex(properties.getProperty("elastic.search.gnd"), properties, log)
    private val wikidataIndex = ElasticIndex(properties.getProperty("elastic.search.wikidata"), properties, log)
    private val dbpediaIndex = ElasticIndex(properties.getProperty("elastic.search.dbpedia"), properties, log)


    fun getEnrichmentData(json: JsonObject): Pair<JsonObject, Map<String, MutableMap<String, Any>>> {
        val resultArray = mutableMapOf<String, MutableMap<String, Any>>(
            Pair("viaf", mutableMapOf()),
            Pair("dbpedia", mutableMapOf()),
            Pair("gnd", mutableMapOf()),
            Pair("wikidata", mutableMapOf())
        )
        val sameAsProperty = json["owl:sameAs"]
        if (sameAsProperty is List<*>)
        {
            for (sameAs in sameAsProperty) {
                if (sameAs is String) {
                    try {
                        val provider = providers.getProvider(sameAs)
                        if (provider.providerSlug == "VIAF") {
                            val response = viafIndex.get("@id", sameAs)
                            if (response.hits.hits.size == 1) {
                                resultArray["viaf"] = response.hits.hits[0].sourceAsMap
                            } else {
                                log.error("Could not find $sameAs in viaf index.")
                            }
                        } else if (provider.providerSlug == "GND") {
                            val response = gndIndex.get("id", sameAs)
                            if (response.hits.hits.size == 1) {
                                resultArray["gnd"] = response.hits.hits[0].sourceAsMap
                            } else {
                                log.error("Could not find $sameAs in gnd index.")
                            }
                        } else if (provider.providerSlug == "WIKIDATA") {
                            val response = wikidataIndex.get("@id", sameAs)
                            if (response.hits.hits.size == 1) {
                                resultArray["wikidata"] = response.hits.hits[0].sourceAsMap
                            } else {
                                log.error("Could not find $sameAs in wikidata index.")
                            }
                        } else if (provider.providerSlug in listOf("DBPEDIA-DE", "DBPEDIA-EN", "DBPEDIA-FR", "DBPEDIA-IT")) {
                            val response = dbpediaIndex.get("@id", sameAs)
                            if (response.hits.hits.size == 1) {
                                val source = response.hits.hits[0].sourceAsMap
                                val target = resultArray["dbpedia"]!!
                                if (source.containsKey("dbo:abstract")) {
                                    if (target.containsKey("dbo:abstract")) {
                                        val existingAbstract = target["dbo:abstract"]!! as Map<String, String>
                                        val newAbstract = source["dbo:abstract"]!! as Map<String, String>
                                        target["dbo:abstract"] = existingAbstract + newAbstract
                                    } else {
                                        target["dbo:abstract"] = source["dbo:abstract"]!!
                                    }
                                }

                                if (source.containsKey("foaf:isPrimaryTopicOf")) {
                                    val exisitingPrimaryTopcis = target["foaf:isPrimaryTopicOf"]
                                    if (exisitingPrimaryTopcis != null) {
                                        target["foaf:isPrimaryTopicOf"] =
                                            (exisitingPrimaryTopcis as List<String>) + source["foaf:isPrimaryTopicOf"]
                                    } else {
                                        target["foaf:isPrimaryTopicOf"] = listOf(source["foaf:isPrimaryTopicOf"]!!)
                                    }
                                }
                            } else {
                                log.error("Could not find $sameAs in dbpedia index.")
                            }
                        }
                    } catch (ex: NoProviderFoundException) {
                        log.error("Unknown uri in sameAs relation: $sameAs")
                    }

                }
            }
        }
        return Pair(json, resultArray)
    }



    fun enrich(swissbibPerson: JsonObject,
               viaf: Map<String, Any>?,
               wikidata: Map<String, Any>?,
               gnd: Map<String, Any>?,
               dbpedia: Map<String, Any>?): Map<String, Any?>  {
        val result = mutableMapOf<String, Any?>()
        for (entry in mergeConfiguration.dataModel) {
            actionLoop@for (action in entry.value.actions) {
                when (action) {
                    "swissbib" -> {
                        val value = swissbibPerson[mergeConfiguration.swissbib[entry.key]]
                        if (value != null) {
                            result[entry.key] = value
                            break@actionLoop
                        }
                    }
                    "viaf" -> {
                        if (viaf != null) {
                            try {
                                val mergedProperties = mergeViafProperty(entry, viaf)
                                if (mergedProperties.isNotEmpty()) {
                                    result.putAll(mergedProperties)
                                    break@actionLoop
                                }
                            } catch (ex: NoProviderFoundException) {
                                log.error(ex.message + "in ${swissbibPerson["@id"]}")
                            }
                        }
                    }
                    "wikidata" -> {
                        if (wikidata != null) {
                            try {
                                val mergedProperties = mergeWikidataProperty(entry, wikidata)
                                if (mergedProperties.isNotEmpty()) {
                                    result.putAll(mergedProperties)
                                    break@actionLoop
                                }
                            } catch (ex: NoProviderFoundException) {
                                log.error(ex.message + "in ${swissbibPerson["@id"]}")
                            }

                        }
                    }
                    "gnd" -> {
                        if (gnd != null) {
                            try {
                                val mergedProperties = mergeGndProperty(entry, gnd)
                                if (mergedProperties.isNotEmpty()) {
                                    result.putAll(mergedProperties)
                                    break@actionLoop
                                }
                            } catch (ex: NoProviderFoundException) {
                                log.error(ex.message + "in ${swissbibPerson["@id"]}")
                            }

                        }
                    }
                    "dbpedia" -> {
                        if (dbpedia != null) {
                            try {
                                val mergedProperties = mergeDbpediaProperty(entry, dbpedia)
                                if (mergedProperties.isNotEmpty()) {
                                    result.putAll(mergedProperties)
                                    break@actionLoop
                                }
                            } catch (ex: NoProviderFoundException) {
                                log.error(ex.message)
                            }
                        }
                    }
                    "concat_unique" -> {
                        result.putAll(concatUniqueMergeStrategy(entry, wikidata, gnd, viaf, dbpedia, swissbibPerson))
                    }
                    "concat_unique_lang" -> {
                        result.putAll(concatUniqueLangMergeStrategy(entry, wikidata, gnd, viaf, dbpedia, swissbibPerson))
                    }
                    else -> log.error("Unknown action $action for merger.")
                }
            }
        }
        return result

    }

    private fun mergeGndProperty(entry: MutableMap.MutableEntry<String, TargetField>,
                                 gnd: Map<String, Any>): MutableMap<String, Any?> {
        val result = mutableMapOf<String, Any?>()
        val value = gnd[mergeConfiguration.gnd[entry.key]]
        if (value != null) {
            if (entry.value.labels) {
                value as List<Map<String, String>>
                val container = mutableListOf<JsonObject>()
                for (item in value) {
                    val gndLabel = GndIdLabelContainer(item["id"] ?: "", item["label"] ?: "")
                    container.add(gndLabel.toIdLanguageContainer().toNoNullValues())
                }
                result[entry.key] = container
            } else {
                when (value) {
                    is String -> result[entry.key] = value
                    is List<*> -> {
                        val output = mutableListOf<String>()
                        innerLoop@for (item in value) {
                            when (item) {
                                is String -> {
                                    if (value.size == 1) {
                                        result[entry.key] = value[0]
                                    } else {
                                        result[entry.key] = value
                                    }
                                    break@innerLoop
                                }
                                is Map<*, *> -> {
                                    if (item.containsKey("id")) {
                                        output.add(item["id"] as String)
                                    }
                                }
                            }
                        }
                        if (output.isNotEmpty()) {
                            result[entry.key] = output
                        }
                    }
                    else -> log.error("Unknown type of content $value for key ${entry.key}")
                }

            }
        }
        return result
    }

    private fun mergeViafProperty(entry: MutableMap.MutableEntry<String, TargetField>,
                                 viaf: Map<String, Any>): MutableMap<String, Any?> {
        val result = mutableMapOf<String, Any?>()
        val value = viaf[mergeConfiguration.viaf[entry.key]] ?: return result
        if (entry.value.labels) {
            val uris = unpackURI(value)
            val containers = mutableListOf<JsonObject>()
            for (uri in uris) {
                val provider = providers.getProvider(uri)
                when (provider.providerSlug) {
                    "VIAF" -> {
                        val response = viafIndex.getLabel(
                            uri,
                            arrayOf("schema:name", "rdfs:label", "schema:alternateName"),
                            emptyArray()
                        )
                        when {
                            response.hits.hits.isEmpty() -> {
                                log.error("Could not find an label for $uri in viaf-all!")
                                containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                            }
                            response.hits.hits.size > 1 -> {
                                log.error("Found more than one entries for viaf id $uri")
                                containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                            }
                            else -> {
                                val names = jsonParser.parseObject<ViafLabels>(response.hits.hits[0].sourceAsString)
                                if (names != null) {
                                    when {
                                        names.name != null -> containers.add(
                                            names.name.simplify().toIdLanguageContainer(uri).toNoNullValues())
                                        names.label != null -> containers.add(IdLanguageContainer(
                                            uri,
                                            names.getRdfLabels()[0],
                                            names.getRdfLabels()[0],
                                            names.getRdfLabels()[0],
                                            names.getRdfLabels()[0]
                                        ).toNoNullValues())
                                        names.alternateName != null -> containers.add(IdLanguageContainer(
                                            uri,
                                            names.getAlternateName(),
                                            names.getAlternateName(),
                                            names.getAlternateName(),
                                            names.getAlternateName()
                                        ).toNoNullValues())
                                        else -> {
                                            log.error("No label for uri $uri.")
                                            containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                                        }

                                    }
                                } else {
                                    log.error("No entry for uri $uri in viaf-all.")
                                    containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                                }
                            }
                        }
                    }
                    else -> log.error("Non VIAF ID $uri in VIAF property ${entry.key}.")
                }
            }
            if (containers.isNotEmpty()) {
                result[entry.key] = containers
            }
        } else {
            result[entry.key] = value
        }
        return result
    }

    private fun mergeWikidataProperty(entry: MutableMap.MutableEntry<String, TargetField>,
                                 wikidata: Map<String, Any>): MutableMap<String, Any?> {
        val result = mutableMapOf<String, Any?>()
        val value = wikidata[mergeConfiguration.wikidata[entry.key]] ?: return result
        if (entry.value.labels) {
            val containers = mutableListOf<JsonObject>()
            val uris = unpackURI(value)
            for (uri in uris) {
                val provider = providers.getProvider(uri)
                when (provider.providerSlug) {
                    "WIKIDATA" -> {
                        val response = wikidataIndex.getLabel(uri, arrayOf("schema:name"), emptyArray())
                        when {
                            response.hits.hits.isEmpty() -> {
                                log.error("Could not find an label for $uri in wikidata-all!")
                                containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                            }
                            response.hits.hits.size > 1 -> {
                                log.error("Found more than one entries for wikidata id $uri")
                                containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                            }
                            else -> {
                                val labels = jsonParser.parseObject<WikidataLabels>(response.hits.hits[0].sourceAsString)
                                if (labels?.label != null) {
                                    containers.add(labels.label.simplify().toIdLanguageContainer(uri).toNoNullValues())
                                } else {
                                    containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                                }
                            }
                        }
                    }
                    else -> log.error("Found Non-Wikidata ID $uri in wikidata property ${entry.key}")
                }
            }
            if (containers.isNotEmpty()) {
                result[entry.key] = containers
            }
        } else {
            result[entry.key] = value
        }
        return result
    }

    private fun mergeDbpediaProperty(entry: MutableMap.MutableEntry<String, TargetField>,
                                     dbpedia: Map<String, Any>): Map<String, Any> {

        val result = mutableMapOf<String, Any>()
        val value = dbpedia[mergeConfiguration.dbpedia[entry.key]] ?: return result

        if (entry.value.labels) {
            val containers = mutableListOf<JsonObject>()
            val uris = unpackURI(value)
            for (uri in uris) {
                val provider = providers.getProvider(uri)
                when {
                    arrayOf("DBPEDIA-EN", "DBPEDIA-FR", "DBPEDIA-IT", "DBPEDIA-DE").contains(provider.providerSlug) -> {
                        val response = viafIndex.getLabel(uri, arrayOf("foaf:name"), emptyArray())
                        when {
                            response.hits.hits.isEmpty() -> {
                                log.error("Could not find an label for $uri in dbpedia-all!")
                                containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                            }
                            response.hits.hits.size > 1 -> {
                                log.error("Found more than one entries for dbpedia id $uri")
                                containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                            }
                            else -> {
                                val labels = jsonParser.parseObject<DbpediaLabel>(response.hits.hits[0].sourceAsString)
                                if (labels?.name != null) {
                                    containers.add(labels.name.simplify().toIdLanguageContainer(uri).toNoNullValues())
                                } else {
                                    containers.add(JsonObject(mutableMapOf(Pair("@id", uri))))
                                }
                            }
                        }

                    }
                    else -> log.error("Found non-DBPEDIA URI $uri in dbpedia property ${entry.key}")
                }
            }
            if (containers.isNotEmpty()) {
                result[entry.key] = containers
            }
        } else {
            result[entry.key] = value
        }
        return result
    }

    private fun unpackURI(value: Any): List<String> {
        val uris = mutableListOf<String>()
        if (value is String) {
            uris.add(value)
        } else if (value is List<*>) {
            if (value[0] is String) {
                value as List<String>
                uris.addAll(value)
            }
        }
        return uris
    }

    private fun concatUniqueMergeStrategy(entry: MutableMap.MutableEntry<String, TargetField>,
                                          wikidata: Map<String, Any>?,
                                          gnd: Map<String, Any>?,
                                          viaf: Map<String, Any>?,
                                          dbpedia: Map<String, Any>?,
                                          swissbibPerson: JsonObject): MutableMap<String, Any?> {

        val result = mutableMapOf<String, Any?>()
        val names = mutableSetOf<String>()
        if (mergeConfiguration.swissbib.containsKey(entry.key)) {
            names.addAll(appendValues(swissbibPerson[mergeConfiguration.swissbib[entry.key]]))
        }
        if (mergeConfiguration.viaf.containsKey(entry.key)) {
            names.addAll(appendValues(viaf?.get(mergeConfiguration.viaf[entry.key])))
        }
        if (mergeConfiguration.gnd.containsKey(entry.key)) {
            names.addAll(appendValues(gnd?.get(mergeConfiguration.gnd[entry.key])))
        }
        if (mergeConfiguration.wikidata.containsKey(entry.key)) {
            names.addAll(appendValues(wikidata?.get(mergeConfiguration.wikidata[entry.key])))
        }
        if (mergeConfiguration.dbpedia.containsKey(entry.key)) {
            names.addAll(appendValues(dbpedia?.get(mergeConfiguration.dbpedia[entry.key])))
        }
        if (names.isNotEmpty())
        {
            result[entry.key] = names
        }
        return result
    }

    private fun appendValues(value: Any?): Set<String> {
        return when (value) {
            is String -> setOf(value)
            is List<*> -> {
                when {
                    value[0] is String -> {
                        value as List<String>
                        value.toSet()
                    }
                    // this case appears from gnd entry wikipedia where we have a dict inside...
                    value[0] is HashMap<*, *> -> {
                        val result = mutableSetOf<String>()
                        for (v in value) {
                            if (v != null && v is HashMap<*, *>) {
                                result.add(v["id"] as String)
                            }
                        }
                        result
                    }
                    else -> {
                        emptySet()
                    }
                }
            }
            else -> emptySet()
        }
    }

    private fun concatUniqueLangMergeStrategy(entry: MutableMap.MutableEntry<String, TargetField>,
                                              wikidata: Map<String, Any>?,
                                              gnd: Map<String, Any>?,
                                              viaf: Map<String, Any>?,
                                              dbpedia: Map<String, Any>?,
                                              swissbibPerson: JsonObject): MutableMap<String, Any?> {
        val result = mutableMapOf<String, Any?>()
        try {
            var resultContainer = SimpleLanguageContainer(emptyList(), emptyList(), emptyList(), emptyList())
            if (viaf != null) {
                val viafKey = mergeConfiguration.viaf[entry.key]
                if (viafKey != null) {
                    val viafContainer = parseContainer(viaf, viafKey)
                    if (viafContainer != null) {
                        resultContainer = resultContainer.mergeLanguageContainers(viafContainer.simplify())
                    }
                }
            }
            if (wikidata != null) {
                val wikidataKey = mergeConfiguration.wikidata[entry.key]
                if (wikidataKey != null) {
                    val wikidataContainer = parseContainer(wikidata, wikidataKey)
                    if (wikidataContainer != null) {
                        resultContainer = resultContainer.mergeLanguageContainers(wikidataContainer.simplify())
                    }
                }
            }
            if (gnd != null) {
                val gndKey = mergeConfiguration.gnd[entry.key]
                if (gndKey != null) {
                    val gndContainer = parseContainer(gnd, gndKey)
                    if (gndContainer != null) {
                        resultContainer = resultContainer.mergeLanguageContainers(gndContainer.simplify())
                    }
                }
            }
            if (dbpedia != null) {
                val dbpediaKey = mergeConfiguration.gnd[entry.key]
                if (dbpediaKey != null) {
                    val dbpediaContainer = parseContainer(dbpedia, dbpediaKey)
                    if (dbpediaContainer != null) {
                        resultContainer = resultContainer.mergeLanguageContainers(dbpediaContainer.simplify())
                    }
                }
            }

            if (!resultContainer.isEmpty()) {
                result[entry.key] = resultContainer.noEmptyLists()
            }
            return result
        } catch (ex: java.lang.ClassCastException) {
            log.error("ClassCastException for swissbib Person: ${swissbibPerson["@id"]}")
            return result
        }
    }


    private fun parseContainer(source: Map<String, Any>, key: String): LanguageContainer? {
        return if (source[key] != null)
            jsonParser.parseObject<LanguageContainer>(Klaxon().toJsonString(source[key]))
        else null
    }



}