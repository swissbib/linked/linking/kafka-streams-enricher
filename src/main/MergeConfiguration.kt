/*
 * kafka-streams-enricher: Enriches swissbib data resources with external data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File
import java.util.*

class MergeConfiguration(properties: Properties, type: String) {

    val dataModel = mutableMapOf<String, TargetField>()
    val swissbib = mutableMapOf<String, String>()
    val gnd = mutableMapOf<String, String>()
    val viaf = mutableMapOf<String, String>()
    val dbpedia = mutableMapOf<String, String>()
    val wikidata = mutableMapOf<String, String>()

    init {
        val path = properties.getProperty("file.csv.$type")!!
        val rows: List<Map<String, String>> = csvReader().readAllWithHeader(File(path))
        var count = 2
        for (line in rows) {
            val outputProperty = line["output"] ?: continue
            if (outputProperty == "") continue
            val action = line["action"] ?: throw Exception("No action defined on line $count for property $outputProperty.")
            val actions = action.split(":")
            val enrichLabel = (line["is_uri_property"] ?: "FALSE").toBoolean()
            dataModel[outputProperty] = TargetField(actions, enrichLabel)

            val swissbibProperty = line["swissbib"]
            if (swissbibProperty != null) swissbib[outputProperty] = swissbibProperty
            val dbpediaProperty = line["dbpedia"]
            if (dbpediaProperty != null) dbpedia[outputProperty] = dbpediaProperty
            val wikidataProperty = line["wikidata"]
            if (wikidataProperty != null) wikidata[outputProperty] = wikidataProperty
            val viafProperty = line["viaf"]
            if (viafProperty != null) viaf[outputProperty] = viafProperty
            val gndProperty = line["gnd"]
            if (gndProperty != null) gnd[outputProperty] = gndProperty
            count += 1
        }
    }
}

data class TargetField(
    val actions: List<String>,
    val labels: Boolean
)