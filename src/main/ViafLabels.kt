/*
 * kafka-streams-enricher: Enriches swissbib data resources with external data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.Json

data class ViafLabels(
    @Json(name = "schema:name")
    val name: LanguageContainer? = null,
    @Json(name = "rdfs:label")
    val label: Any? = null,
    @Json(name = "schema:alternateName")
    val alternateName: Any? = null
) {
    fun getAlternateName(): String {
        return when (alternateName) {
            is String -> alternateName
            is List<*> -> alternateName[0] as String
            else -> throw Exception("This should not have happened: schema:alternateName is neither a single string nor a list of strings.")
        }
    }

    fun getRdfLabels(): List<String> {
        return when (label) {
            is String -> listOf(label)
            is List<*> -> label as List<String>
            else -> throw Exception("rdfs:label was neither list nor string... $label")
        }
    }
}