/*
 * kafka-streams-enricher: Enriches swissbib data resources with external data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.Json
import com.beust.klaxon.JsonObject

data class LanguageContainer(
    val en: Any? = null,
    @Json(name = "en-GB")
    val en_GB: Any? = null,
    @Json(name = "en-gb")
    val en_gb: Any? = null,
    @Json(name = "en-ca")
    val en_ca: Any? = null,
    @Json(name = "en-US")
    val en_us: Any? = null,
    val de: Any? = null,
    @Json(name = "de-AT")
    val de_AT: Any? = null,
    @Json(name = "de-at")
    val de_at: Any? = null,
    @Json(name = "de-CH")
    val de_CH: Any? = null,
    @Json(name = "de-ch")
    val de_ch: Any? = null,
    @Json(name = "de-DE")
    val de_de: Any? = null,
    val it: Any? = null,
    @Json(name = "it-VA")
    val it_va: Any? = null,
    @Json(name = "it-IT")
    val it_it: Any? = null,
    @Json(name = "it-CH")
    val it_ch: Any? = null,
    val fr: Any? = null,
    @Json(name = "fr-FR")
    val fr_fr: Any? = null,
    @Json(name = "fr-CH")
    val fr_ch: Any? = null
) {

    private fun getListValue(value: Any?): List<String> {
        return when (value) {
            is String -> listOf(value)
            is List<*> -> value as List<String>
            else -> emptyList()
        }
    }

    fun simplify(): SimpleLanguageContainer {
        val concatEn = getListValue(en) +
                getListValue(en_GB) +
                getListValue(en_ca) +
                getListValue(en_gb) +
                getListValue(en_us)
        val resultEnglish = mutableSetOf<String>()
        resultEnglish.addAll(concatEn)

        val concatDe = getListValue(de) +
                getListValue(de_AT) +
                getListValue(de_CH) +
                getListValue(de_at) +
                getListValue(de_de) +
                getListValue(de_ch)
        val resultGerman = mutableSetOf<String>()
        resultGerman.addAll(concatDe)

        val concatFr = getListValue(fr) +
                getListValue(fr_ch) +
                getListValue(fr_fr)
        val resultFrench = mutableSetOf<String>()
        resultFrench.addAll(concatFr)

        val concatIt = getListValue(it) +
                getListValue(it_it) +
                getListValue(it_ch) +
                getListValue(it_va)
        val resultItalian = mutableSetOf<String>()
        resultItalian.addAll(concatIt)

        return SimpleLanguageContainer(
            resultEnglish.toList(),
            resultFrench.toList(),
            resultGerman.toList(),
            resultItalian.toList()
            )
    }
}

data class GndIdLabelContainer(
    val id: String,
    val label: String
) {
    fun toIdLanguageContainer(): IdLanguageContainer {
        return IdLanguageContainer(id, null, null, label, null)
    }
}

data class SimpleLanguageContainer(
    val en: List<String>,
    val fr: List<String>,
    val de: List<String>,
    val it: List<String>)  {

    fun toIdLanguageContainer(uri: String): IdLanguageContainer {
        return IdLanguageContainer(
            uri,
            if (en.isNotEmpty()) en[0] else null,
            if (fr.isNotEmpty()) fr[0] else null,
            if (de.isNotEmpty()) de[0] else null,
            if (it.isNotEmpty()) it[0] else null
        )
    }

    fun mergeLanguageContainers(other: SimpleLanguageContainer): SimpleLanguageContainer {
        return SimpleLanguageContainer(
            (other.en.toSet() + en.toSet()).toList(),
            (other.fr.toSet() + fr.toSet()).toList(),
            (other.de.toSet() + de.toSet()).toList(),
            (other.it.toSet() + it.toSet()).toList())
    }

    fun isEmpty(): Boolean {
        return it.isEmpty() && en.isEmpty() && de.isEmpty() && fr.isEmpty()
    }

    fun noEmptyLists(): JsonObject {
        val returnValue = JsonObject()
        if (it.isNotEmpty()) returnValue["it"] = it
        if (de.isNotEmpty()) returnValue["de"] = de
        if (fr.isNotEmpty()) returnValue["fr"] = fr
        if (en.isNotEmpty()) returnValue["en"] = en
        return returnValue
    }
}

data class IdLanguageContainer(
    val id: String,
    val en: String?,
    val fr: String?,
    val de: String?,
    val it: String?
) {

    fun toNoNullValues(): JsonObject {
        val returnValue = JsonObject()
        if (en != null) returnValue["en"] = en
        if (de != null) returnValue["de"] = de
        if (fr != null) returnValue["fr"] = fr
        if (it != null) returnValue["it"] = it
        returnValue["@id"] = id
        return returnValue
    }
}