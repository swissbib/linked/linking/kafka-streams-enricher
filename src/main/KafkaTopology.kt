/*
 * kafka-streams-enricher: Enriches swissbib data resources with external data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.util.*
import java.util.regex.Pattern

class KafkaTopology(private val properties: Properties, private val log: Logger) {

    private val persons = Pattern.compile("http://xmlns.com/foaf/0.1/Person")
    private val organisations = Pattern.compile("http://xmlns.com/foaf/0.1/Organization")
    private val pattern = Pattern.compile("owl:sameAs")

    private val personsEnricher = Enricher(MergeConfiguration(properties,"persons"), properties, log)
    private val organisationsEnricher = Enricher(MergeConfiguration(properties,"organisations"), properties, log)
    private val parser = JsonParser(log)

    private val sink = properties.getProperty("kafka.topic.sink")

    fun build(): Topology {
        val builder = StreamsBuilder()

        val indexActionBranch = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .branch(
                Predicate { _, value -> value.esBulkAction == EsBulkActions.INDEX }//,
                //Predicate { _, value -> value.esBulkAction == EsBulkActions.DELETE }
            )

        val enrichable = indexActionBranch[0]
            .branch(
                Predicate { _, value -> pattern.matcher(value.data).find() },
                Predicate { _, _ -> true }
            )

        val typeBranch = enrichable[0]
            .branch(
                Predicate { _, value -> persons.matcher(value.data).find() },
                Predicate { _, value -> organisations.matcher(value.data).find() }
            )

        typeBranch[0]
            .flatMapValues { value -> extractParseResult(value) }
            .mapValues { value -> getPersonsEnrichmentMaps(value) }
            .mapValues { value -> mergePersons(value) }
            .mapValues { value -> createMetadataModel(value.first, value.second) }
            .to(sink)

        typeBranch[1]
            .flatMapValues { value -> extractParseResult(value) }
            .mapValues { value -> getOrganisationsEnrichmentMaps(value) }
            .mapValues { value -> mergeOrganisations(value) }
            .mapValues { value -> createMetadataModel(value.first, value.second) }
            .to(sink)



        // delete actions are simply forwarded to the output topic.
        // currently ignored. Needs a mechanic to check if this person should actually be deleted.
        //indexActionBranch[1]
        //    .to(sink)

        // forward any records without present links!
        enrichable[1]
            .to(sink)

        return builder.build()
    }

    private fun getPersonsEnrichmentMaps(value: Pair<String, JsonObject>): Pair<String, Pair<JsonObject, Map<String, Map<String, Any>?>>> {
        return Pair(value.first, personsEnricher.getEnrichmentData(value.second))
    }

    private fun getOrganisationsEnrichmentMaps(value: Pair<String, JsonObject>): Pair<String, Pair<JsonObject, Map<String, Map<String, Any>?>>> {
        return Pair(value.first, organisationsEnricher.getEnrichmentData(value.second))
    }

    private fun mergePersons(values: Pair<String, Pair<JsonObject, Map<String, Map<String, Any>?>>>): Pair<String, Map<String, Any?>> {
        return Pair(values.first, personsEnricher.enrich(values.second.first,
            values.second.second["viaf"],
            values.second.second["wikidata"],
            values.second.second["gnd"],
            values.second.second["dbpedia"]))
    }

    private fun mergeOrganisations(values: Pair<String, Pair<JsonObject, Map<String, Map<String, Any>?>>>): Pair<String, Map<String, Any?>> {
        return Pair(values.first, organisationsEnricher.enrich(values.second.first,
            values.second.second["viaf"],
            values.second.second["wikidata"],
            values.second.second["gnd"],
            values.second.second["dbpedia"]))
    }

    private fun createMetadataModel(index: String, data: Map<String, Any?>): SbMetadataModel {
        return SbMetadataModel()
            .setData(Klaxon().toJsonString(data))
            .setEsIndexName(index)
            .setEsBulkAction(EsBulkActions.INDEX)
    }

    private fun extractParseResult(value: SbMetadataModel): List<Pair<String, JsonObject>> {
        val parsedObject = parser.parse(value.data)
        return if (parsedObject != null) {
            listOf(Pair(value.esIndexName, parsedObject))
        } else {
            listOf()
        }
    }

    // obsolete implementation of a full merge for analytical purposes.
    private fun fullMerge(values: Pair<JsonObject, Map<String, Map<String, Any>?>>): Map<String, Any?> {
        val person = values.first;
        val gnd = values.second["gnd"]
        val viaf = values.second["viaf"]
        val wikidata  = values.second["wikidata"]
        val result = mutableMapOf<String, Any?>()

        for (key in person.keys) {
            result["sb_$key"] = person[key]
        }
        if (gnd != null) {
            for (key in gnd.keys) {
                result["gnd_$key"] = gnd[key]
            }
        }
        if (viaf != null) {
            for (key in viaf.keys) {
                result["viaf_$key"] = viaf[key]
            }
        }
        if (wikidata != null) {
            for (key in wikidata.keys) {
                result["wd_$key"] = wikidata[key]
            }
        }
        return result
    }

}
