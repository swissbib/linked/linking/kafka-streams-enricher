/*
 * kafka-streams-enricher: Enriches swissbib data resources with external data
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.types.EsBulkActions
import java.io.File
import java.nio.charset.Charset

class Tests {
    private val log = LogManager.getLogger()
    private val props = KafkaProperties(log)
    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties);

    private val resourcePath = "src/test/resources"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @Test
    fun testCase1() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"), "https://data.swissbib.ch/person/83a23fae-8727-3676-b9b0-7ce412f73fa1",
            SbMetadataModel().setData(readFile("input1.json")).setEsIndexName("sb-persons-20-20-2020").setEsBulkAction(
            EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertAll("test jean le clerc",
            { assertEquals("https://data.swissbib.ch/person/83a23fae-8727-3676-b9b0-7ce412f73fa1", output.key())},
            { assertEquals("sb-persons-20-20-2020", output.value().esIndexName)},
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)},
            { assertEquals(readFile("output1.json"), output.value().data)}
        )
    }

    @Test
    fun testCase2() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/3f5ee0ba-198a-3ba2-af87-05ea78b5d8ea",
            SbMetadataModel().setData(readFile("input2.json")).setEsIndexName("sb-persons-20-20-2020").setEsBulkAction(
            EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())


        assertAll("test shakespear william",
            { assertEquals("https://data.swissbib.ch/person/3f5ee0ba-198a-3ba2-af87-05ea78b5d8ea", output.key()) },
            { assertEquals("sb-persons-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output2.json"), output.value().data) }
        )
    }

    @Test
    fun testCase3() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/89426dbb-c6be-3e92-a566-8ec1ec00044a",
            SbMetadataModel().setData(readFile("input3.json")).setEsIndexName("sb-persons-20-20-2020").setEsBulkAction(
            EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertAll("test marvin murdick",
            { assertEquals("https://data.swissbib.ch/person/89426dbb-c6be-3e92-a566-8ec1ec00044a", output.key()) },
            { assertEquals("sb-persons-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output3.json"), output.value().data) }
        )
    }

    @Test
    fun testCase4() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/52cefdd3-f0ef-378a-a63e-139c187c9333",
            SbMetadataModel().setData(readFile("input4.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
            EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertAll("test Association pour la sauvegarde du patrimoine rural jurassien",
            { assertEquals("https://data.swissbib.ch/person/52cefdd3-f0ef-378a-a63e-139c187c9333", output.key()) },
            { assertEquals("sb-organisations-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output4.json"), output.value().data) }
            )
    }



    @Test
    fun testCase5() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/05ac89d0-3651-3fcf-8d26-2ff697e032ef",
            SbMetadataModel().setData(readFile("input5.json")).setEsIndexName("sb-persons-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertAll("test Jane Austen",
            { assertEquals("https://data.swissbib.ch/person/05ac89d0-3651-3fcf-8d26-2ff697e032ef", output.key()) },
            { assertEquals("sb-persons-20-20-2020", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(readFile("output5.json"), output.value().data) }
        )

    }



    @Test
    fun testCase6() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/6cf6cffa-1c2f-3ab7-b5b0-eeeb0d0f07c0",
            SbMetadataModel().setData(readFile("input6.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://data.swissbib.ch/person/6cf6cffa-1c2f-3ab7-b5b0-eeeb0d0f07c0", output.key())
        assertEquals("sb-organisations-20-20-2020", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output6.json"), output.value().data)

    }
    @Test
    fun testCase7() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/73d1bbe6-7611-31cc-90a4-ad9bcbf9e0de",
            SbMetadataModel().setData(readFile("input7.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://data.swissbib.ch/person/73d1bbe6-7611-31cc-90a4-ad9bcbf9e0de", output.key())
        assertEquals("sb-organisations-20-20-2020", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output7.json"), output.value().data)

    }
    @Test
    fun testCase8() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/c28ca1e5-d9ca-38d5-a698-70e30fe6ca80",
            SbMetadataModel().setData(readFile("input8.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://data.swissbib.ch/person/c28ca1e5-d9ca-38d5-a698-70e30fe6ca80", output.key())
        assertEquals("sb-organisations-20-20-2020", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output8.json"), output.value().data)
    }

    @Test
    fun testCase9() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/c28ca1e5-d9ca-38d5-a698-70e30fe6ca80",
            SbMetadataModel().setData(readFile("input9.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://data.swissbib.ch/person/c28ca1e5-d9ca-38d5-a698-70e30fe6ca80", output.key())
        assertEquals("sb-organisations-20-20-2020", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output9.json"), output.value().data)
    }

    @Test
    fun testCase10() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/person/72c23b30-6df9-3fb3-b3b8-55ed127ba4c0",
            SbMetadataModel().setData(readFile("input10.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://data.swissbib.ch/person/72c23b30-6df9-3fb3-b3b8-55ed127ba4c0", output.key())
        assertEquals("sb-organisations-20-20-2020", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output10.json"), output.value().data)
    }

    @Test
    fun testCase11() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>( StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(factory.create(props.appProperties.getProperty("kafka.topic.source"),
            "https://data.swissbib.ch/organisation/3c226fd8-b991-3dec-ad46-bc1709f6b92f",
            SbMetadataModel().setData(readFile("input11.json")).setEsIndexName("sb-organisations-20-20-2020").setEsBulkAction(
                EsBulkActions.INDEX)))
        val output =
            testDriver.readOutput(props.appProperties.getProperty("kafka.topic.sink"), StringDeserializer(), SbMetadataDeserializer())

        assertEquals("https://data.swissbib.ch/organisation/3c226fd8-b991-3dec-ad46-bc1709f6b92f", output.key())
        assertEquals("sb-organisations-20-20-2020", output.value().esIndexName)
        assertEquals(EsBulkActions.INDEX, output.value().esBulkAction)
        assertEquals(readFile("output11.json"), output.value().data)
    }
}