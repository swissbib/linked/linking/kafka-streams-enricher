## Swissbib Enricher

Mirco service to use owl:sameAs links to enrich data from dbpedia, gnd, viaf & wikidata.

The enrichment mapping is based on two files. One for persons and one for organisations.

- [Persons Mapping](https://gitlab.com/swissbib/linked/workflows/blob/master/linking/sb-enricher/configs/VERZ_persons_20191104_jow.csv).
- [Organisations Mapping](https://gitlab.com/swissbib/linked/workflows/blob/master/linking/sb-enricher/configs/VERZ_organisations_20191104_jow.csv).

The mapping determines the names of the source properties. The name of the target 
property and how these are merged.

**IMPORTANT: The mappings defined inside of the src/test/resources folder are only 
for testing. They will not be included in the container image.** 

Currently we have three merge strategies:

- priority list: a list of source labels separated by colons. The enrichment process
will try each source in turn and stop as soon as an enrichment is found.
- concat_unique: all sources are collected and combined into a set of values.
- concat_unique_lang: all sources are collected and combined into a set of values per
language (en, de, fr, it).

